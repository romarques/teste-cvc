var Location = require('../model/location');

module.exports = function(app) {

  app.get('/api/locations', function(request, response) {
    Location.find({})
    .exec(function(err, locations) {
        if (err) {
          response.status(500).json({ info: "Fail" });
        }

        response.status(200).json(locations);
    });
  });

  app.post('/api/locations', function(request, response) {
    var santoAndre = new Location({ name: 'Santo André - São Paulo/BR, Brasil' });
    var guarulhos = new Location({ name: 'Guarulhos - São Paulo/BR, Brasil' });
    var saoPaulo = new Location({ name: 'Santo André - São Paulo/BR, Brasil' });

    santoAndre.save(function(err) {
      guarulhos.save(function(err) {
        saoPaulo.save(function(err) {
            response.status(201).json('ok');
        });
      });
    });
  });

};
