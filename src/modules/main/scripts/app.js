'use strict';

require('!style!css!resolve-url!sass?sourceMap!../styles/main.scss');

var loader = require('./loader');
var modules = loader.loadModules();

var app = angular.module('app', [
    'ui.router',
    'oc.lazyLoad',
    'ui.bootstrap',
    'ngStorage',
    'toastr',
    'angularMoment',
    'angular-loading-bar',
    'jcs-autoValidate',
    'xeditable',
    'ui.utils.masks',
    'mwl.confirm',
    'infinite-scroll',
    'treeGrid',
    'ngFileUpload'
].concat(modules));

loader.loadComponents(app);

module.exports = app;
