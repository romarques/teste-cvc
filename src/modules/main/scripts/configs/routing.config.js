'use strict';

function Routing($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/home');

    $stateProvider.state('app', {
        abstract: true,
        url: '/app',
        controller: 'layoutController as shell',
        template: require('../../templates/layout.template.html')
    })
    .state('app.home', {
        url: '/home',
        displayName: 'Cars',
        views: {
            container: {
                controller: 'modulesController as vm',
                templateProvider: function ($q) {
                    return $q(function (resolve) {
                        require.ensure([], function (require) {
                            var template = require('../../templates/home.template.html');
                            resolve(template);
                        }, 'home');
                    });
                }
            }
        }
    });

}

module.exports = function (app) {
    app.config(Routing);
};
