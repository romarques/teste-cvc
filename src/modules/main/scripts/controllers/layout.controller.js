"use strict";

function LayoutController($state, $window, $timeout, cfpLoadingBar) {
    this.$state = $state;
    this.$window = $window;
    this.$timeout = $timeout;

    var shell = this;

}

LayoutController.$inject = ['$state', '$window', '$timeout', 'cfpLoadingBar'];

module.exports = function (app) {
    app.controller('layoutController', LayoutController);
};
