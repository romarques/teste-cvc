'use strict';

function ModulesController(carService) {
    this.carService = carService;
    this.cars = [];
    this.initialize();
}

ModulesController.prototype.initialize = function() {
    this.cars = this.carService.get();
};

ModulesController.$inject = ['carService'];

module.exports = function(app) {
    app.controller('modulesController', ModulesController);
};
