'use strict';
function RouteService($http, $q, $state, API) {
    this.$http = $http;
    this.$q = $q;
    this.$state = $state;
    this.API = API;
}

RouteService.prototype.goToRoute = function(linkUrl, parameter, reload) {
    return this.$state.go(linkUrl, parameter, {reload: reload});
};

RouteService.prototype.goToModule = function(module, linkUrl, parameter) {
    return this.$state.go(module + '.' + linkUrl, parameter);
};

RouteService.prototype.makeRoute = function(linkUrl) {
    return module.name + '.' + linkUrl;
};

module.exports = function(app) {
    app.service('routeService', RouteService);
};
