'use strict';

var app = require('../app');

function LocationService($http) {
  this.$http = $http;

  // this.locations = [
  //   'São Paulo - São Paulo/BR, Brasil',
  //   'Guarulhos - São Paulo/BR, Brasil',
  //   'Santo André - São Paulo/BR, Brasil'
  // ];
}

LocationService.prototype.get = function() {
  return this.$http.get('/api/locations');
};

module.exports = function(app) {
    app.service('locationService', LocationService);
};
