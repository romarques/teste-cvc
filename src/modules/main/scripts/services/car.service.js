'use strict';

var app = require('../app');

function CarService() {
  this.cars = [
    {
      includedTax: true,
      discount: 20,
      discountValue: 1239,
      installments: 2,
      value: 1000,
      brandImage: require('../../images/logo-hertz.png'),
      carImage: require('../../images/renault-sandero.png'),
      title: 'CARRO ECONÔMICO COM AR',
      description: 'CHEVROLET CELTA, FORD FIESTA, VW GOL, FIAT PALIO OU SIMILAR',
      optionalDescription: 'Super inclusive promocional - Km livre, seguro total de veículo (LDW), seguro a terceiros (SLI), taxas de serviços inclusas e 1 motorista adicional.',
      descriptionTopics: [
        'Quilômetragem livre',
        'Seguro total do veículo',
        'Seguro à terceiros',
        'Taxas de serviços inclusas'
      ],
      tax: {
        code: 'IFMR '
      },
      optionals: {
        fourDoors: true,
        airConditioning: true,
        electricGlass: true,
        fiveSeats: true,
        automatic: true,
        cdUsb: true,
        threeBags: true,
        hydraulicSteering: true,
        amFm: true
      }
    },
    {
      installments: 12,
      value: 838,
      brandImage: require('../../images/logo-budget.png'),
      carImage: require('../../images/renault-sandero.png'),
      title: 'CARRO ECONÔMICO COM AR',
      description: 'CHEVROLET CELTA, FORD FIESTA, VW GOL, FIAT PALIO OU SIMILAR',
      optionalDescription: 'Super inclusive promocional - Km livre, seguro total de veículo (LDW), seguro a terceiros (SLI), taxas de serviços inclusas e 1 motorista adicional.',
      descriptionTopics: [
        'Quilômetragem livre',
        'Seguro total do veículo',
        'Seguro à terceiros',
        'Taxas de serviços inclusas'
      ],
      tax: {
        code: 'IFMR '
      },
      optionals: {
        airConditioning: true,
        electricGlass: true,
        automatic: true,
        cdUsb: true,
        hydraulicSteering: true,
        amFm: true
      }
    }
  ];
}

CarService.prototype.get = function() {
    return this.cars;
};

module.exports = function(app) {
    app.service('carService', CarService);
};
