function FavoriteController(toastr) {
  var vm = this;

  vm.class = "red";

  vm.changeClass = function() {
    if (vm.class === "red") {
      vm.class = "grey";
      toastr.success('Carro removido dos favoritos');
    } else {
      vm.class = "red";
      toastr.success('Carro adicionado aos favoritos');
    }
  };
}

function FavoriteDirective() {
    return {
        restrict: 'E',
        replace: true,
        template: require('../../templates/directives/favorite.template.html'),
        controller: ['toastr', FavoriteController],
        controllerAs: 'vm'
    };
}


module.exports = function (app) {
    app.directive('favoriteDirective', FavoriteDirective);
};
