function SearchController(locationService, $scope) {
  $scope.locations = [];

  locationService.get()
    .then(function(response) {
      if (response.data) {
        $scope.locations = response.data.map(function(location) {
          return location.name;
        });
      }
    });

  $scope.dateOptions = {
    formatYear: 'yyyy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  $scope.samePlace = true;

  $scope.withdrawalDate = null;
  $scope.withdrawalDateOpened = false;

  $scope.openwithdrawalDate = function() {
    $scope.withdrawalDateOpened = true;
  };

  $scope.returnDate = null;
  $scope.returnDateOpened = false;

  $scope.openReturnDate = function() {
    $scope.returnDateOpened = true;
  };

  $scope.onRetiradaChanged = function() {
    if ($scope.samePlace === true) {
      $scope.devolucao = '';
    }
  };
}

function SearchDirective() {
    return {
        restrict: 'E',
        replace: true,
        template: require('../../templates/directives/search.template.html'),
        controller: ['locationService', '$scope', SearchController]
    };
}


module.exports = function (app) {
    app.directive('searchDirective', SearchDirective);
};
