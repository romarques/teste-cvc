module.exports = function (app) {
    var baseUrlApi = '/api';

    app.constant('API', baseUrlApi);
};
