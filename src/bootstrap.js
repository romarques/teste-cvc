'use strict';

var app = require('./modules/main/scripts/app');

angular.element(document).ready(function() {
    angular.bootstrap(document, [app.name]);
});
