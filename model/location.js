var mongoose = require('mongoose');

var location_schema = mongoose.Schema({
    name: String
});

module.exports = mongoose.model('Location', location_schema);
