var path = require('path'),
    webpack = require('webpack'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    OpenBrowserPlugin = require('open-browser-webpack-plugin'),
    ExtractTextPlugin = require("extract-text-webpack-plugin");

const paths = {
    context: path.join(__dirname, '/src'),
    build: path.join(__dirname, '/public'),
    nodeModules: path.join(__dirname, '/node_modules')
};

var config = {
    context: paths.context,
    entry: {
        app: [
            './bootstrap.js'
        ],
        vendor: [
            'angular',
            'angular-ui-router',
            'angular-ui-bootstrap',
            'oclazyload',
            'lodash',
            'expose?$!expose?jQuery!jquery',
            'font-awesome-webpack',
            'bootstrap-webpack!./modules/main/styles/config/bootstrap.config.js',
            'angular-toastr',
            'ngstorage',
            'moment',
            'angular-moment',
            'moment/locale/pt-br.js',
            'angular-i18n/angular-locale_pt-br.js',
            'angular-loading-bar',
            'angular-auto-validate/dist/jcs-auto-validate.min.js',
            'angular-xeditable',
            'angular-input-masks',
            'angular-bootstrap-confirm',
            'ng-infinite-scroll',
            'angular-bootstrap-grid-tree/src/tree-grid-directive.js',
            'ng-file-upload/dist/ng-file-upload.min.js',
            'ng-file-upload/dist/ng-file-upload-shim.min.js'
        ]
    },
    output: {
        path: paths.build,
        filename: 'app.bundle.[hash].js',
        chunkFilename: "[name].bundle.[hash].js",
        sourceMapFilename: '[file].map'
    },
    module: {
        loaders: [{
            test: /\.html$/,
            loader: 'html'
        }, {
            test: /\.js$/,
            loader: 'ng-annotate'
        }, {
            test: /\.css$/,
            loader: "style!css?sourceMap"
        }, {
            test: /\.less$/,
            loader: 'style!css?sourceMap!less'
        }, {
            test: /\.scss$/,
            loader: 'style!css?sourceMap!sass'
        }, {
            test: /\.json$/,
            loader: 'json'
        }, {
            test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url?limit=10000&mimetype=application/font-woff&name=./fonts/[hash].[ext]'
        }, {
            test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url?limit=10000&mimetype=application/octet-stream&name=./fonts/[hash].[ext]'
        }, {
            test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'file?name=./fonts/[hash].[ext]'
        }, {
            test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url?limit=10000&mimetype=image/svg+xml&name=./imgs/[hash].[ext]'
        }, {
            test: /\.(jpe?g|png|gif)$/i,
            loader: 'file?hash=sha512&digest=hex&name=./imgs/[hash].[ext]'
        }],
    },
    plugins: [
        new OpenBrowserPlugin({ url: 'http://localhost:8081' }),
        new HtmlWebpackPlugin({
            inject: true,
            showErrors: true,
            title: 'Teste CVC',
            template: './index.ejs'
        }),
        new webpack.DefinePlugin({
            PROCESS_CWD: JSON.stringify(process.cwd())
        }),
        new webpack.EnvironmentPlugin([
            "NODE_ENV"
        ]),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: Infinity,
            filename: "[name].bundle.[hash].js",
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
            output: {
                comments: false,
            }
        }),
        new webpack.HotModuleReplacementPlugin()
    ],
    resolve: {
        alias: {
            loader$: paths.context + "/scripts/loader.js"
        }
    },
    devtool: "#eval",
    devServer: {
        hot: true,
        inline: true,
        contentBase:'./'
    }
};

module.exports = config;
