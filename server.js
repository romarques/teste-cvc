var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var morgan = require('morgan');
var config = require('./config/configuration');

mongoose.connect(config.mongo);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(morgan('dev'));

app.use(express.static(__dirname + "/public"));

var location = require('./routes/location')(app);

app.listen(process.env.PORT || 3000, function(){
  console.log("Teste CVC rodando");
});
